package sdp.common;

public enum RotationDirection {
    CLOCKWISE, COUNTERCLOCKWISE;
}
