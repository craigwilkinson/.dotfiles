package sdp.robot;

import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;


public class Sensors extends Thread{

	private TouchSensor rightSensor = new TouchSensor(SensorPort.S1);
	private TouchSensor leftSensor = new TouchSensor(SensorPort.S2);
	private Robot robot;
	public boolean active = false;

	public Sensors(Robot robot){
		this.robot = robot;
		start();
	}
	
	public boolean isActive() {  //checks if this thread is active
		return active;
	}
	
	public void run(){
		while(true) {
			try {
				if (rightSensor.isPressed()||leftSensor.isPressed()){
					this.active = true;
					System.out.println("Sensors are pressed!");
					robot.stopMoving();
					robot.stopRotating();
					robot.move((float)Math.PI);
					System.out.println("Moved back");
					Thread.sleep(250);  //adjust to set how long it travels back
					robot.stopMoving();
					this.active = false;
				}
			} catch (InterruptedException e) {
				System.out.println("Error in sensors.");
			}
		}
	}
	
}
