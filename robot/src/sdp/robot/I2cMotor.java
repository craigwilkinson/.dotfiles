package sdp.robot;

import lejos.nxt.I2CSensor;

public class I2cMotor{
	public static final int MAX_MOTOR_SPEED = 255;
	private static final byte I2C_OFF = 0;
	private static final byte I2C_FORWARD = 1;
	private static final byte I2C_BACKWARD = 2;
	
	double mWheelAngle;
	int mDirectionRegister;
	I2CSensor mSensor;
	
    int mWheelDirectionModifier;

	public I2CSensor getSensor(){
		return mSensor;
	}
	public double getWheelAngle(){
		return mWheelAngle;
	}
    public int getWheelDirectionModifier(){
        return mWheelDirectionModifier;
    }
	
	public void setSpeed(int speed){
		if ( Math.abs(speed) > 255 ){
			throw new IllegalArgumentException("Speed exceeds bounds of 255");
		}else if ( speed > 0 ){
			mSensor.sendData(mDirectionRegister, I2C_FORWARD);
			mSensor.sendData(mDirectionRegister + 1, (byte)speed);
		}else if ( speed == 0 ){
			mSensor.sendData(mDirectionRegister, I2C_OFF);
		}else{
			speed = -speed;
			mSensor.sendData(mDirectionRegister, I2C_BACKWARD);
			mSensor.sendData(mDirectionRegister + 1, (byte)speed);
		}
	}
	
	/**
	 * 
	 * @param sensor The sensor that the motor is controlled by
	 * @param directionRegister The register to set the direction. The speed is assumed to be
	 * 							the next register
	 * @param wheelAngle The angle of the forward direction of the wheel from the "forward"
	 * 					 of the robot
     * @param wheelDirectionModifier a multiplier (-1/1) depending on the direction of the wheel. 
     *                               If the "forward" is facing clockwise, then 1, otherwise -1
	 */
	public I2cMotor(I2CSensor sensor,
					int directionRegister,
					double wheelAngle,
					int wheelDirectionModifier){
        if ( wheelDirectionModifier != 1 && wheelDirectionModifier != -1 ){
            throw new IllegalArgumentException("wheelDirection must be 0 or 1");
        }
		mSensor = sensor;
		mWheelAngle = wheelAngle;
		mDirectionRegister = directionRegister;
        mWheelDirectionModifier = wheelDirectionModifier;
	}
}