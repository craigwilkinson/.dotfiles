package sdp.robot.test;

import sdp.robot.*;
import org.junit.Test;

public class I2cMotorTests {
	
	@Test(expected = IllegalArgumentException.class)
	public void setSpeed_WhenSetSpeedAbove225_Exception(){
		new I2cMotor(null, 0, 0.0, 0).setSpeed(256);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void setSpeed_WhenSetSpeedBelow225_Exception(){
		new I2cMotor(null, 0, 0.0, 0).setSpeed(-256);
	}
}
