package communication;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/*
 * "And a thimble's worth of milky moon,
 * can touch hearts larger than a thimble."
 * 
 * 		- Joanna Newsom
 */

/**
 * Allows other systems to communicate with the robot via a TCP socket
 * 
 * @author Sarun Gulyanon
 * @author Richard Kenyon
 * 
 */
public class CommunicationServer extends Thread {
	private ServerSocket serverSocket;
	private Socket socket;
	private InputStream in;
	private Command command;

	private static final String NXT_MAC_ADDRESS = "00:16:53:08:AD:03";
	private static final String NXT_NAME = "Sexy Robot";
	private static final int PORT = 5678;
	private static final int RECONNECT_WAIT_MILLIS = 1000;

	private static BluetoothCommunication comms;

	/**
	 * Makes a Bluetooth connection then opens a TCP socket to
	 * listen for commands from the Strategy system.
	 */
	public CommunicationServer() {
		command = new Command();

		System.out.println("Starting server...");
		comms = new BluetoothCommunication(NXT_NAME, NXT_MAC_ADDRESS);
		try {
			comms.openBluetoothConnection();
		} catch (IOException e) {
            System.err.print(e);
			System.err.println("Couldn't make bluetooth connection");
		}
		try {
			serverSocket = new ServerSocket(PORT);
		} catch (IOException e) {
			System.err.println("Couldn't Create Server Socket on Port " + PORT);
		}
		
		openTCPConnection();
				

		start(); // starts a thread which sends commands to the robot
		
		PingSender pinger = new PingSender(command, 2000);
		pinger.start();		
		
		listen(); // listen for commands from Strategy
	}

	/**
	 * Listens to the socket for commands and forwards any commands it receives
	 * to the robot
	 */
	private void listen() {
		
		outerloop:
		for (;;) {
			if (socket.isClosed()) {
				openTCPConnection();
			}

			try {
				byte[] commandFromClient = new byte[Constants.COMMAND_SIZE];

				int bytesRead = 0;
				
				while ( bytesRead < Constants.COMMAND_SIZE ){
					int numofBytesRead = in.read(commandFromClient, bytesRead, Constants.COMMAND_SIZE - bytesRead);
					if ( numofBytesRead == -1 ){
						System.err.println("Closed socket");
						closeTCPConnection();
						continue outerloop;
					}
					bytesRead += numofBytesRead;
				}

				byte[] floatBytes = {commandFromClient[1],
						             commandFromClient[2],
						             commandFromClient[3],
						             commandFromClient[4]};
	            DataInputStream dis = new DataInputStream(new ByteArrayInputStream(floatBytes));
	            
				if (bytesRead == Constants.COMMAND_SIZE) {
					System.out.println("Got command " + commandFromClient[0] + "(" + dis.readFloat() + ")");
					command.set(commandFromClient);
				}
			} catch (IOException e) {
				System.err.println("Couldn't read from socket, "
						+ "closing socket...");
				closeTCPConnection();
			}
		}
	}

	/**
	 * Get the latest command and sends it to the robot.
	 * 
	 * If the connection goes down this method will attempt to reconnect
	 */
	public void run() {

		byte[] commandToSend;

		for (;;) {
			commandToSend = command.get();
			
			checkConnection();

			try {
				comms.sendToRobot(commandToSend);
			} catch (IOException e) {
				System.err.println("Lost bluetooth connection to robot");
				comms.closeBluetoothConnection();

			} catch (IllegalArgumentException e) {
				System.err.println("Server cannot send command: "
						+ e.toString());
			}
			
			checkConnection();
		}
	}
	
	public void checkConnection(){
		// If connection is down, then reconnect
		while (!comms.hasConnection()) {
			System.out.println("Waiting to reconnect...");

			// Wait a few seconds before attempting
			// to reconnect to the robot
			try {
				Thread.sleep(RECONNECT_WAIT_MILLIS);
			} catch (InterruptedException e) {
				System.err.println("Sleep interrupted");
			}

			// Try re-establishing Bluetooth connection
			try {
				comms.openBluetoothConnection();
			} catch (IOException e) {
				System.err.println("Couldn't make bluetooth connection");
			}
		}
	}

	/**
	 * Open TCP connection and connects the input and output streams to this new
	 * TCP connection
	 */
	private void openTCPConnection() {
		try {
			socket = serverSocket.accept();
			System.out.println("Socket accepted connection");
		} catch (IOException e) {
			System.err.println("Couldn't listen on port: " + PORT);
		}
		// Open Socket Input/Output Stream
		try {
			in = socket.getInputStream();
		} catch (IOException e) {
			System.err.println("Error opening input/output stream");
		}
	}

	/**
	 * Close TCP connection and close input and output streams
	 */
	private void closeTCPConnection() {
		try {
			in.close();
			socket.close();
		} catch (IOException e) {
			System.err.println("Couldn't close TCP connection");
		}
	}

	/**
	 * Starts an instance of the Communication Server.
	 * @param args
	 */
	public static void main(String args[]) {
		System.out.println("Starting communication server...");
		new CommunicationServer();
	}
}
