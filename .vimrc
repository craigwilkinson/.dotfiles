syntax on

colorscheme craigs-light

" show line numbers
set number

" mad tab stuff
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab

" Hide the menu bars
set guioptions=M
set guioptions-=T
set guioptions-=r
set guioptions-=L

" scroll a little from the boom
set scrolloff=5

" folding
set foldmethod=syntax
set foldlevelstart=1

let javaScript_fold=1         " JavaScript
let perl_fold=1               " Perl
let php_folding=1             " PHP
let r_syntax_folding=1        " R
let ruby_fold=1               " Ruby
let sh_fold_enabled=1         " sh
let vimsyn_folding='af'       " Vim script
let xml_syntax_folding=1      " XML

set laststatus=2
set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P
