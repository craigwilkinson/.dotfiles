ECLIPSE SETUP
--------------
Create a new project with the *pc* directory as the project location (you may have to uncheck 'use default location')

Right click on the project you have just created and press properties
Now go to Java Build Path
Then select the libraries tab
Click "Add external JARs..." and add all the jars found in /group/teaching/sdp/sdp1/lib/

Go to the run configurations for your new project (create it if it doesn't exist) and add a new environmental variable `LD_LIBRARY_PATH` with the value `/group/teaching/sdp/sdp1/lib`.

VISION FEATURE IDEAS
--------------------

* Change/add filtering of outlier points?
* Combine vision with a simulated guess - feed all motions into simulator, guess/predict where objects will be next, storing momentums. 
Average this guess location with the vision output, giving the simulation a larger weighting when vision is giving an erratic location? 
Sim should be given a weighting that is given a sudden boost when the vision gives an erratic/conflicting location, and then falls exponentially 
till boosted again? Implement simulator in 'WorldState' class, use the 'MovingObject' class
* The red ball should be found before the background is subtracted, as there was previously no trouble finding the ball
* Create other feature dimensions - such as 'red minus green'
* blue and yellow could suppress each other by proximity, as a blue and yellow patch should never be recognised within 
the same panel
* Initially, a sparse sweep to locate the plates and crosses. Then take the mean location, and scan this box properly.
* do a 4-mean clustering on the green plate, to identify the 4 quarters?
